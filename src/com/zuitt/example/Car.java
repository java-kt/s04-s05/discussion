package com.zuitt.example;

public class Car {
    //Access Modifier
    // These are used to restrict the scope of a class, constructor, variable, method or data member.
    // Four Types of Access Modifiers:
    //1. Default - No Keyword indicated (accessibility is within the package)
    //2. Private - Properties or method only accessible within the class.
    //3. Protected - Properties and methods are only accessible by the class of the same package and the subclass present in any package.
    //4. Public - Properties and methods can be accessed from anywhere.

    // Class creation
    // Four parts of class creation
    // 1. Properties - Characteristics of an object.

    private String name;
    private String brand;
    private int yearOfMake;
    // Make a component of a car
    private Driver driver;

    // 2. Constructor - used to create/instantiate an object.

    // a. empty constructor - creates object that doesn't have any arguments/parameters
    public Car(){
        // set a default value upon instantiation
        this.yearOfMake = 2000;
        // Whenever a new car is created, it will have a driver named "Alejandro"
        this.driver = new Driver("Alejandro");
    }

    // b. parameterized constructor - creates an object with arguments/parameters.
    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Alejandro");
    }

    // 3. Getters and Setters - get and set the values of each property of the object.

    // Getters - retrieve the value of instantiated object
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getYearOfMake(){
        return this.yearOfMake;
    }
    public String getDriverName(){
        // this will invoke that getName() of Driver Class.
        return this.driver.getName();
    }

    // Setters - use to change the default value of an instantiated object.
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYearOfMake(int yearOfMake){
        // can also be modified to add data validation.
        if(yearOfMake <=2022){
            this.yearOfMake = yearOfMake;
        }
    }

    public void setDriver(String driver){
        // this will invoke the setName() of the Driver Class
        this.driver.setName(driver);
    }

    // 4. Methods - functions an object can perform (action).
    public void drive(){
        System.out.println("The car is running. Vroom. Vroom.");
    }
}
